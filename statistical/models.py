from django.db import models
from django.contrib.auth.models import User
from django.contrib.postgres.fields import JSONField
from upload.models import Document
	
class statistical(models.Model):
	statistical_id = models.CharField(max_length = 250, null=False, blank=True)
	statistical_name = models.CharField(max_length = 250, null=False, blank=True)
	statistical_method = models.CharField(max_length = 250, null=False, blank=True)
	# statistical_calculated_value=models.DecimalField(decimal_places=5,max_digits=19)
	statistical_calculated_value = JSONField(default={})
	user_id = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True,default=0)
	dataset_id = models.ForeignKey(Document, on_delete=models.CASCADE, blank=True, null=True)
	parameters = JSONField(default={})