import plotly.graph_objs as go
import plotly.plotly as py

import numpy as np

colorscale = [[0, '#FAEE1C'], [0.33, '#F3558E'], [0.66, '#9C1DE7'], [1, '#581B98']]
trace1 = go.Scatter(
    y = np.random.randn(500),
    mode='markers',
    marker=dict(
        size='16',
        color = np.random.randn(500),
        colorscale=colorscale,
        showscale=True
    )
)
data = [trace1]
url_1 = py.plot(data, filename='scatter-for-dashboard', auto_open=False)

print('url',url_1)
py.iplot(data, filename='scatter-for-dashboard')



1. dashboard_id
2. dashboardname
3. dashboardtype - landscape, potrait

header
textbox
graph1
textbox2
text3
graph2
text4
footer

header
textbox1
graph1header
graph1
textbox2
header
textbox3
graph2
textbox4
footer


tabel2
1. dashboard_id
2. pagenumber
3. boxnumber
4. boxtype- header, footer, text,visualization,statistical summary, dataset view
5. 